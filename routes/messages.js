'use strict';

const express = require('express');
const router = express.Router();
const mongoose = require('./../database/mongoProvider');
const crypto = require('crypto');
let dbP;

mongoose.mongo.connect(mongoose.url, function(err, db){
    if(!err){
        dbP = db;
    }
});

router.get('/:hash', function(req, res) {
    dbP.collection("hashes").findOne({'hex': req.params.hash}, function(err, data){
        if(!err){
            console.log(data);
            res.send(data.message);
        }else{
            console.log(data);
            res.send(404);
        }
    });
});

/* GET users listing. */
router.post('/', function(req, res) {
    let data = "";
    let object;
    let sha = crypto.createHash("sha256");

    req.on("data", function(chunk){
        data+=chunk;
    });

    req.on("end", function(){
        object = JSON.parse(data);
        sha.update(object.digest);
        let hex = sha.digest('hex');
        dbP.collection("hashes").update({'message': object.digest}, {'hex':hex, 'message': object.digest}, {upsert: true}, function(err, resp){
            if(err){
                res.send(501);
            }else{
                console.log("SAVED TO DB. " + hex + " for " + object.digest);
                res.send(hex);
            }
        });
    });
});


module.exports = router;
