'use strict';

let mongo = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/hash";

mongo.connect(url, function(err, db){
    if(!err){
        db.createCollection('hashes');
    }
});

module.exports = {'mongo':mongo, 'url': url};
